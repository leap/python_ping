-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: python-ping
Binary: python-ping
Architecture: all
Version: 0.2-1
Maintainer: Kristina Clair <kclair@leap.se>
Standards-Version: 3.8.4
Build-Depends: debhelper (>= 7), python-support (>= 0.8.4)
Checksums-Sha1: 
 54b7ece233794e06193bfc0983117259c554eed0 10979 python-ping_0.2.orig.tar.gz
 75f5a6bb4302e3be2ad827053d2b562c719c9adc 11225 python-ping_0.2-1.debian.tar.gz
Checksums-Sha256: 
 ecb32294c2af8ae075de4a3743f568db376480ad81c2e010a7f1ce1cee7b030f 10979 python-ping_0.2.orig.tar.gz
 c80a002fabe73635d7449106aaeb9c30369c6ec034297fcd9253a758edabedfc 11225 python-ping_0.2-1.debian.tar.gz
Files: 
 57228e54268390ca2fef5181d5568e38 10979 python-ping_0.2.orig.tar.gz
 65ef7c284d45cad2c5b74a49348eae4b 11225 python-ping_0.2-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: GPGTools - http://gpgtools.org

iQIcBAEBAgAGBQJQbItwAAoJEJ6m/xxX04TjZ2sQAJmBmP7E8og9okK2A+tI+u3v
heH4t2SXd3L380Q30i0vIb+iA6iUCh11Ba15D3CO6+UT2JoFq6TSCNk+xdq95oHf
AeW7KVDR/BoT44PexZ/akSz9rPWKJfe2fLobEMmGY2Y5Lwe19fqiea/48DxCJHip
C6LWBrGokh5LRfQz5f5dIcpgUIJ7M5pRvW3IJhG68fQvf9RAwAY8CjAxkleibF7w
b8Y7INuI595mI+jzBrmBrs/F9orEaQj1+Wuqyo/cDwcglQ+oy3VigZPrCXj7xr/f
tO83eRkWim/uE5hocKufyQVz9Pd7fAf8Gk3NwaS9xDMWKNOzph62ab1MI1GybIOd
MCV8KniDHOm3zHc5klInpOUvW5mN8GdCixKRzc4tnlamhFIQFotdY6MwfVcWvfaP
NeWkzgfjOfhEgsZZxRM9wonQWghbuFXFyRnI1+oIfNSLRRSSh3Egj/mdlq3wpLAZ
D384AHxjoy0ynr8zPpu4m54qSfg6XiHTUKzo311FwwmBjaQm+NVbkLIuaKCnCJ7Q
z6X7X0A8vt+HIjkUvbEUcKIE59p8YJtwySx9XZ5vUbs8i65Awjo/6jqP7Vmvzdv2
ojyW1tpdEFuY/mCpYWsSrm3v1SNavCRshFL92ZouW5HdVFfRggQ3J3c7TvbRbVpP
BTsim4Ef3lxXhLSJwVeS
=4n+5
-----END PGP SIGNATURE-----
